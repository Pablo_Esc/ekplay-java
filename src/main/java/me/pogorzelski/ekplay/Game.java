package me.pogorzelski.ekplay;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.HashMap;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;

public class Game {

    public Request request;
    public String userName;
    public static Boolean isLocked;
    public Integer counter = 1001;
    public static final Logger logger = LogManager.getLogger(Logger.class
            .getName());

    public Game() {
        request = new Request();
    }

    public static Boolean getIsLocked() {
        return isLocked;
    }

    public static void setIsLocked(Boolean isLocked) {
        Game.isLocked = isLocked;
    }

    public String makeInstantGameRequest(String page, String action, HashMap<String, String> parameters, Boolean debug) throws IOException, InterruptedException {
        return makeGameRequest(page, action, parameters, "instant", debug);
    }

    public String makeFastGameRequest(String page, String action, HashMap<String, String> parameters, Boolean debug) throws IOException, InterruptedException {
        return makeGameRequest(page, action, parameters, "fast", debug);
    }

    public String makeRegularGameRequest(String page, String action, HashMap<String, String> parameters, Boolean debug) throws IOException, InterruptedException {
        return makeGameRequest(page, action, parameters, "regular", debug);
    }

    public String makeGameRequest(String page, String action, HashMap<String, String> parameters, String waitType, Boolean debug) throws IOException, InterruptedException {
        logger.info("Making game request");
        String parsedParameters = "";
        if (parameters != null && parameters.size() > 0) {
            parsedParameters = "&";
            for (String parameter : parameters.keySet()) {
                parsedParameters += parameter + "=" + parameters.get(parameter)
                        + "&";
            }
            parsedParameters = parsedParameters.substring(0,
                    parsedParameters.length() - 1);
        }

        String url = Config.gameUrl
                + page
                + ".php?do="
                + action
                + parsedParameters
                + "&v="
                + String.valueOf(counter)
                + "&phpp=ANDROID_ARC&phpl=EN&pvc=1.7.4&pvb=2015-11-07%2018%3A55";
        logger.info(url);
        logger.info(request);
        String result = request.makePost(url, null);
        counter++;
        Thread.sleep(Config.getRequestWaitTime(waitType));
        if (debug) {
            Util.dumpToFile(url, "debug.output");
            Util.dumpToFile(result, "debug.output");
            Util.dumpToFile("----------------------------------",
                    "debug.output");
        }
        return result;
    }

    public void scheduleEvents() {

        JobDetail job = JobBuilder.newJob(DemonInvasion.class)
                .withIdentity("diJob", "diGroup").build();

        // Trigger trigger = TriggerBuilder
        // .newTrigger()
        // .withIdentity("diTrigger", "diGroup")
        // .startAt(triggerStartTime)
        // .
        // .build();
        // schedule it
        // Scheduler scheduler = new StdSchedulerFactory().getScheduler();
        // scheduler.start();
        // // scheduler.scheduleJob(job, trigger);
    }

    public void init() throws IOException, ConfigurationException,
            InterruptedException, SQLException, ParseException,
            SchedulerException {
        setIsLocked(false);
        // Boolean diRunning = Config.checkDiRunning();
        // logger.info(diRunning);
        // if (diRunning) {
        // Calendar calendar = Calendar.getInstance();
        // calendar.add(Calendar.SECOND, 5);
        // DemonInvasion.scheduleRound(calendar.getTime());
        // }

    }

    public void play() throws IOException, ConfigurationException,
            InterruptedException, SQLException, ParseException,
            SchedulerException {

        // init();
        // getUserDecks();
        // testRequest();
        // makeGameRequest("card", "GetUserCards", null, true);
        // try {
        // // for (HttpCookie cookie :
        // // request.cookieManager.getCookieStore().getCookies()) {
        // // logger.info(cookie);
        // // }
        // game.testRequest(request);
        // } catch (IOException e) {
        // e.printStackTrace();
        // } catch (InterruptedException e) {
        // // TODO Auto-generated catch block
        // e.printStackTrace();
        // }
    }

    public void testRequest() throws IOException, InterruptedException {
//        HashMap<String, String> parameters = new HashMap<>();
//        parameters.put("Amount", "10");
//        parameters.put("StartRank", "1");
//        makeRegularGameRequest("boss", "GetRanks", parameters, true);
//
//        makeRegularGameRequest("card", "GetUserCards", null, true);

        HashMap<String, String> parameters2 = new HashMap<>();
        parameters2.put("Type", "1");
        parameters2.put("UserCardId1", "9200");
        parameters2.put("UserCardId2", "17865169");
        parameters2.put("SkillId", "167");
        makeRegularGameRequest("streng", "Evolution", parameters2, true);
//        makeRegularGameRequest("mapstage", "ExplorePre", null, true);
//        makeRegularGameRequest("arena", "GetRankCompetitors", null, true);

//		makeGameRequest("hydra", "GetHydraInfo", null, true, true);
        // makeGameRequest("boss", "GetRanks", null, true);
        // makeGameRequest("boss", "Fight", null, true);
        // makeGameRequest("boss", "GetFightData", null, true);
        // makeGameRequest(request, "card", "GetUserCards", null, true);
        // makeGameRequest("league", "lotteryInfo", null, true);
        // // makeGameRequest(request, "task", "GetDailyTaskInfo", null, true);
        // // makeGameRequest(request, "shopnew", "GetGoods", null, true);
        // // makeGameRequest(request, "hydra", "GetHydraInfo", null, true);
        // makeGameRequest("activity", "ActivityInfo", null, true, true);
        // // makeGameRequest(request, "card", "GetCardGroup", null, true);
        // // makeGameRequest(request, "boss", "GetBoss", null, true);
        // // makeGameRequest(request, "arena", "GetThieves", null, true);
//		makeGameRequest("mapstage", "GetUserMapStages", null, true, true);
        // // makeGameRequest(request, "rune", "GetAllRune", null, true);
        // // makeGameRequest(request, "mapstage", "GetMapStageALL", null,
        // true);
//        HashMap<String, String> requestParam = new HashMap<String, String>();
//        requestParam.put("MapStageDetailId", "1");
//        makeGameRequest("mapstage", "GetMapStageDetailInfo", requestParam,
//                true, true);
//        makeGameRequest("mapstage", "GetUserMapStageInfo", requestParam, true,
//                true);
//        makeGameRequest("mapstage", "GetUserMapStageDetailInfo", requestParam,
//                true, true);
        // makeGameRequest("mapstage", "Info", requestParam, true);
    }
}
