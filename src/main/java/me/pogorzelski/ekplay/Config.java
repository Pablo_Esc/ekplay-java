package me.pogorzelski.ekplay;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Random;

import javafx.scene.control.TextArea;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonValue;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class Config {

    public static Integer requestRegularTime = 5000;
    public static Integer requestFastTime = 2500;
    public static Integer requestInstantTime = 0;
    private static Game game = null;
    public static Integer timeOffset = 0;
    public static Scheduler scheduler = null;
    public static String gameUrl = "http://s1.ek.ifreeteam.com/";
    public static String databaseUrl = "jdbc:sqlite:./ekdata.db";
    public static ConnectionSource connectionSource = null;
    public static PropertiesConfiguration propertiesHandle = null;
    public static Config current = null;
    public static Boolean isAuth = false;
    public static Request request;
    public static List<Card> demonList = new ArrayList<>();
    private static Boolean lockedProcess;

    public static TextArea textAreaMainLog = null;
    public static TextArea textAreaDiLog = null;
    public static TextArea textAreaThiefLog = null;
    public static TextArea textAreaArenaLog = null;
    public static TextArea textAreaPlayerData = null;
    public static TextArea textAreaDecks = null;

    public static final Logger logger = LogManager.getLogger(Logger.class
            .getName());

    public static Config getCurrent() {
        return current;
    }

    public static void setCurrent(Config current) {
        Config.current = current;
    }

    public static Integer getTimeOffset() {
        return timeOffset;
    }

    public static void setTimeOffset(Long offset) {
        Config.timeOffset = offset.intValue();
    }

    public static Integer getRequestWaitTime(String type) {
        Random generator = new Random();
        if (type.equals("instant")) {
            return requestInstantTime + generator.nextInt(100);
        } else if (type.equals("fast")) {
            return requestFastTime + generator.nextInt(500);
        } else if (type.equals("regular")) {
            return requestRegularTime + generator.nextInt(4000);
        }
        return 1000;
    }

    /**
     * @return the game
     */
    public static Game getGame() {
        return game;
    }

    /**
     * @param aGame the game to set
     */
    public static void setGame(Game aGame) {
        game = aGame;
    }

    /**
     * @return the lockedProcess
     */
    public static Boolean getLockedProcess() {
        return lockedProcess;
    }

    /**
     * @param aLockedProcess the lockedProcess to set
     */
    public static void setLockedProcess(Boolean aLockedProcess) {
        lockedProcess = aLockedProcess;
    }

    public Config() throws SQLException, IOException, SchedulerException,
            ConfigurationException, InterruptedException, ParseException {
        logger.info("Initializng config");
        // initialize db
        connectionSource = (ConnectionSource) new JdbcConnectionSource(databaseUrl);
        setLockedProcess(false);
        DataAccess dataAccess = new DataAccess();
        Request request = new Request();

        // init request
        Config.request = request;

        // check existence of properties file
        checkProperties();

        // start the scheduler
        startScheduler();
        // set config handler
        propertiesHandle = new PropertiesConfiguration("ekplay.properties");

        // init the game
        setGame(new Game());

        // check data update
        checkDataUpdate(false);

        // set demon list
        demonList = DataAccess.cardDao.queryForEq("type", "100");

        setCurrent(this);

        logger.info("Config initialized");
    }

    public static void checkProperties() throws IOException {
        File propertiesFile = new File("ekplay.properties");
        if (!propertiesFile.exists()) {
            propertiesFile.createNewFile();
        }
    }

    public static void startScheduler() throws SchedulerException {
        SchedulerFactory schedFact = new org.quartz.impl.StdSchedulerFactory();
        Scheduler sched = schedFact.getScheduler();
        sched.start();
        scheduler = sched;
    }

    public static void calcTimeOffset() throws IOException,
            InterruptedException, ParseException {
        String eventsData = getGame().makeInstantGameRequest("activity", "ActivityInfo", null, false);
        String offset = Json.parse(eventsData).asObject().getString("servertime", "NaN");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-kk-mm-ss");
        Date serverTime = formatter.parse(offset);
        Date currentTime = new Date();
        setTimeOffset(currentTime.getTime() - serverTime.getTime() - 1000);
    }

    public static void checkDataUpdate(Boolean forced) throws IOException,
            InterruptedException, SQLException, ParseException {
        if (DataAccess.cardDao.countOf() > 0 && forced.equals(false)) {
            return;
        }
        if (isAuth == false) {
            Auth auth = new Auth();
            auth.process(request);
        }
        logger.info("Updating card database");
        String cards = getGame().makeRegularGameRequest("card", "GetAllCard", null, false);
        String runes = getGame().makeRegularGameRequest("rune", "GetAllRune", null, false);
        String skills = getGame().makeRegularGameRequest("card", "GetAllSkill", null, false);

        JsonArray skillsData = Json.parse(skills).asObject().get("data").asObject().get("Skills").asArray();
        JsonArray cardsData = Json.parse(cards).asObject().get("data").asObject().get("Cards").asArray();
        JsonArray runesData = Json.parse(runes).asObject().get("data").asObject().get("Runes").asArray();
        if (skillsData.size() > DataAccess.skillDao.countOf()
                || cardsData.size() > DataAccess.cardDao.countOf()
                || runesData.size() > DataAccess.runeDao.countOf()) {
            TableUtils.clearTable(Config.connectionSource, Skill.class);
            TableUtils.clearTable(Config.connectionSource, Rune.class);
            TableUtils.clearTable(Config.connectionSource, Card.class);

            // insert all the skills
            for (JsonValue skillJson : skillsData) {
                Skill skill = new Skill();
                skill.setName(skillJson.asObject().getString("Name", "Nan"));
                skill.setId(skillJson.asObject().getInt("SkillId", 0));
                logger.info("Inserting : " + skill.getName() + " at "
                        + skill.getId());
                DataAccess.skillDao.create(skill);
            }

            // insert all the runes
            for (JsonValue runeJson : runesData) {
                Rune rune = new Rune();
                rune.setName(runeJson.asObject().getString("RuneName", "NaN"));
                rune.setId(Integer.valueOf(runeJson.asObject().getString(
                        "RuneId", "NaN")));
                logger.info("Inserting : " + rune.getName() + " at "
                        + rune.getId());
                DataAccess.runeDao.create(rune);
            }

            // insert all the cards
            for (JsonValue cardJson : cardsData) {
                Card card = new Card();
                card.setName(cardJson.asObject().getString("CardName", "NaN"));
                card.setId(Integer.valueOf(cardJson.asObject().getString(
                        "CardId", "NaN")));
                card.setType(cardJson.asObject().getString("Race", "NaN"));
                DataAccess.cardDao.create(card);

                List<String> skillKeys = new ArrayList<>(Arrays.asList("Skill",
                        "LockSkill1", "LockSkill2"));

                skillKeys
                        .stream()
                        .forEach(
                                (skillKey) -> {
                                    try {
                                        if (!cardJson.asObject()
                                        .getString(skillKey, "NaN")
                                        .equals("")) {
                                            for (int i = 0; i < skillKey
                                            .split("_").length; i++) {
                                                String splitSkill = cardJson
                                                .asObject()
                                                .getString(skillKey,
                                                        "NaN")
                                                .split("_")[i];
                                                logger.info("Got split in "
                                                        + splitSkill);

                                                Skill skill = DataAccess.skillDao.queryForId(Integer
                                                        .valueOf(splitSkill));
                                                CardSkill cardSkill = new CardSkill();
                                                cardSkill.setCard(card);
                                                cardSkill.setSkill(skill);
                                                logger.info("Inserting skill : "
                                                        + skill.getName()
                                                        + " at "
                                                        + card.getName());
                                                DataAccess.cardSkillDao
                                                .create(cardSkill);
                                            }
                                        }
                                    } catch (NumberFormatException | SQLException e) {
                                        logger.info("Problem in EK card : "
                                                + card.getName() + " / "
                                                + skillKey + " " + e.toString());
                                    }
                                });
            }
        }
    }
}
