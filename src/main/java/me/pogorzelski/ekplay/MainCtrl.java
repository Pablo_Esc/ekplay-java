package me.pogorzelski.ekplay;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;

import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import org.apache.commons.configuration.ConfigurationException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.SchedulerException;

@SuppressWarnings("restriction")
public class MainCtrl implements Initializable {

    @FXML
    public Tab tabDiDecks;

    @FXML
    public Tab tabInfo;

    @FXML
    public TextArea textAreaMainLog;
    @FXML
    public TextArea textAreaDiLog;
    @FXML
    public TextArea textAreaThiefLog;
    @FXML
    public TextArea textAreaArenaLog;

    @FXML
    public TextArea textAreaPlayerData;

    @FXML
    public TextArea textAreaDecks;

    @FXML
    public VBox vBoxDemonDecks;

    @FXML
    public VBox vBoxOtherDecks;

    public static final Logger logger = LogManager.getLogger(Logger.class.getName());
    public ComboBox<Deck> comboThiefDeck;
    public ComboBox<Deck> comboArenaDeck;

    Task<Boolean> authTask = new Task<Boolean>() {
        @Override
        protected Boolean call() throws Exception {
            Util.addToLog(textAreaMainLog, "Authenticating into the game");
            Request request = new Request();
            Auth auth = new Auth();
            try {
                auth.process(request);
            } catch (IOException | InterruptedException ex) {
                java.util.logging.Logger.getLogger(MainCtrl.class.getName())
                        .log(Level.SEVERE, null, ex);
            }
            return null;
        }

        @Override
        protected void succeeded() {
            super.succeeded();
            Util.addToLog(textAreaMainLog, "Properly authenticated into the game");
        }

    };

    Service<Boolean> authService = new Service<Boolean>() {
        @Override
        protected Task<Boolean> createTask() {
            return authTask;
        }

        ;

    };

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        try {
            initControls();
        } catch (InterruptedException | SQLException | IOException ex) {
            java.util.logging.Logger.getLogger(MainCtrl.class.getName()).log(
                    Level.SEVERE, null, ex);
        }

    }

    public void showDecks() throws NumberFormatException, IOException,
            InterruptedException, SQLException {

    }

    public void initDemonDecks() {
        vBoxDemonDecks.setPadding(new Insets(10));
        vBoxDemonDecks.setSpacing(5);
        vBoxDemonDecks.setAlignment(Pos.CENTER);
        for (Card demon : Config.demonList) {
            HBox box = new HBox();
            box.setPadding(new Insets(10, 10, 10, 10));
            box.setSpacing(10);
            box.setAlignment(Pos.CENTER);
            Label label = new Label();
            label.setText(demon.getName());
            box.getChildren().add(label);
            ComboBox<Deck> comboBox = new ComboBox();
            comboBox.getItems().setAll(User.decks);

            if (Config.propertiesHandle.containsKey(demon.getName()) && !Config.propertiesHandle.getProperty(demon.getName()).equals("0")) {
                String property = (String) Config.propertiesHandle.getProperty(demon.getName());
                Integer deckId = Integer.valueOf(property);
                for (Deck deck : User.decks) {
                    if (deck.getId().equals(deckId)) {
                        comboBox.setValue(deck);
                    }
                }
            }

            box.getChildren().add(comboBox);
            vBoxDemonDecks.getChildren().add(box);
        }
    }

    public void initOtherDecks() {
        comboArenaDeck.getItems().setAll(User.decks);
        if (Config.propertiesHandle.containsKey("arena-deck")) {
            String thiefDeck = (String) Config.propertiesHandle.getProperty("arena-deck");
            Integer deckId = Integer.valueOf(thiefDeck);
            for (Deck deck : User.decks) {
                if (deck.getId().equals(deckId)) {
                    comboArenaDeck.setValue(deck);
                }
            }
        }
        comboThiefDeck.getItems().setAll(User.decks);
        if (Config.propertiesHandle.containsKey("thief-deck")) {
            String thiefDeck = (String) Config.propertiesHandle.getProperty("thief-deck");
            Integer deckId = Integer.valueOf(thiefDeck);
            for (Deck deck : User.decks) {
                if (deck.getId().equals(deckId)) {
                    comboThiefDeck.setValue(deck);
                }
            }
        }
    }

    public void saveDecks() throws ConfigurationException {
        saveDiDecks();
        saveOtherDecks();
        Config.propertiesHandle.save();
        Util.addGeneralLog("Decks saved");
    }

    public void initSettingsControls() {
        initDemonDecks();
        initOtherDecks();
    }

    public void saveDiDecks() throws ConfigurationException {
        String demonName = "";
        Integer demonDeckId = null;
        for (Node node : vBoxDemonDecks.getChildren()) {
            if (node instanceof HBox) {
                for (Node hBoxNode : ((HBox) node).getChildren()) {
                    if (hBoxNode instanceof Label) {
                        demonName = ((Label) hBoxNode).getText();
                    } else if (hBoxNode instanceof ComboBox) {
                        if (((ComboBox) hBoxNode).getSelectionModel().getSelectedItem() != null) {
                            Deck demonDeck = (Deck) ((ComboBox) hBoxNode).getSelectionModel().getSelectedItem();
                            demonDeckId = demonDeck.getId();
                        } else {
                            demonDeckId = 0;
                        }
                    }
                }
                Config.propertiesHandle.setProperty(demonName, demonDeckId);
            }
        }
    }

    public void saveOtherDecks() {
        if (comboArenaDeck.getSelectionModel().getSelectedItem() != null) {
            Deck arenaDeck = comboArenaDeck.getSelectionModel().getSelectedItem();
            Config.propertiesHandle.setProperty("arena-deck", arenaDeck.getId());
        }
        if (comboThiefDeck.getSelectionModel().getSelectedItem() != null) {
            Deck thiefDeck = comboThiefDeck.getSelectionModel().getSelectedItem();
            Config.propertiesHandle.setProperty("thief-deck", thiefDeck.getId());
        }
    }

    public void getUserData() throws IOException, InterruptedException, NumberFormatException, SQLException {
        logger.info(Config.getCurrent());
        logger.info(Config.getGame());
        User.updateUserData();
        User.fillUserDecks();
        initSettingsControls();
    }

    public void initControls() throws InterruptedException, SQLException, IOException {
        Config.textAreaMainLog = textAreaMainLog;
        Config.textAreaDiLog = textAreaDiLog;
        Config.textAreaThiefLog = textAreaThiefLog;
        Config.textAreaPlayerData = textAreaPlayerData;
        Config.textAreaDecks = textAreaDecks;
        Config.textAreaArenaLog = textAreaArenaLog;
    }

    public void connect() throws IOException, InterruptedException {
        if (!Config.isAuth) {
            authService.start();
        }
    }

    public void testRequest() throws IOException, InterruptedException {
        Game game = Config.getGame();
        game.testRequest();
    }

    public void runThieves() throws SchedulerException, IOException, InterruptedException {
        Thief thief = new Thief();
        thief.init(Config.getGame());
    }

    public void runArena() throws SchedulerException, IOException, InterruptedException {
        Arena arena = new Arena();
        arena.init(Config.getGame());
    }

    public void runDI() throws IOException, InterruptedException, SchedulerException, NumberFormatException, SQLException {
        DemonInvasion demonInvasion = new DemonInvasion();
        demonInvasion.init(Config.getGame());
//        Trigger trigger = newTrigger().withIdentity("diTrigger", "diGroup")
//                .startNow().build();
//        Config.scheduler.scheduleJob(DemonInvasion.diJob, trigger);
        // Calendar calendar = Calendar.getInstance();
        // calendar.add(Calendar.SECOND, 5);
        // DemonInvasion.scheduleRound(calendar.getTime());
    }

}
