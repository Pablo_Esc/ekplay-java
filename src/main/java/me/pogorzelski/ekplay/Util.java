package me.pogorzelski.ekplay;

import com.eclipsesource.json.JsonObject;
import javafx.application.Platform;
import javafx.scene.control.TextArea;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by pablo on 04.12.15.
 */
public class Util {

    public static void dumpToFile(String text, String filename)
            throws IOException {
        File dumpFile = new File(filename);
        if (!dumpFile.exists()) {
            dumpFile.createNewFile();
        }
        Files.write(Paths.get(filename), (text + "\n").getBytes(),
                StandardOpenOption.APPEND);

    }

    @SuppressWarnings("restriction")
    public static void addToLog(TextArea textArea, String text) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();

        Platform.runLater(() -> textArea.appendText("\n"
                + dateFormat.format(cal.getTime()) + " : " + text));
    }

    public static void addDiLog(String text) {
        addToLog(Config.textAreaMainLog, text);
        addToLog(Config.textAreaDiLog, text);
    }

    public static void addThiefLog(String text) {
        addToLog(Config.textAreaMainLog, text);
        addToLog(Config.textAreaThiefLog, text);
    }

    public static void addArenaLog(String text) {
        addToLog(Config.textAreaMainLog, text);
        addToLog(Config.textAreaArenaLog, text);
    }

    public static void addGeneralLog(String text) {
        addToLog(Config.textAreaMainLog, text);
    }

    public static String getJsonData(JsonObject jsonObject, String key) {
        String outputValue = "";
        try {
            outputValue = jsonObject.getString(key, "NaN");
        } catch (Exception e) {
            outputValue = String.valueOf(jsonObject.getInt(key, 0));
        }
        return outputValue;
    }
}
