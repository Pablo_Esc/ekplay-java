package me.pogorzelski.ekplay;

import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.zip.GZIPInputStream;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Request {

    public static final Logger logger = LogManager.getLogger(Logger.class.getName());
    public CookieManager cookieManager = new CookieManager();

    public Request() {
        CookieManager reqCookieManager = new CookieManager();
        reqCookieManager.setCookiePolicy(CookiePolicy.ACCEPT_ALL);
        CookieHandler.setDefault(reqCookieManager);
        cookieManager = reqCookieManager;
    }

    public HttpURLConnection createConnection(String url) throws IOException {
        URL objUrl = new URL(url);
        HttpURLConnection connection = (HttpURLConnection) objUrl.openConnection();
        connection.setRequestMethod("GET");
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/27.0.1453.94 Safari/537.36");
        connection.setRequestProperty("Referer", Auth.arcFormUrl);
        connection.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
        connection.setRequestProperty("Accept-Encoding", "gzip,deflate");
        connection.setRequestProperty("Accept-Charset", "ISO-8859-1,utf-8;q=0.7,*;q=0.3");
        return connection;
    }

    public String makeGet(String url) throws IOException {
        HttpURLConnection connection = createConnection(url);
        connection.setRequestProperty("Referer", Auth.arcFormUrl);
        return make(connection);
    }

    public String makePost(String url, HashMap<String, String> postData) throws IOException {
        HttpURLConnection connection = createConnection(url);

        String parameters = "";
        if (postData != null && postData.size() > 0) {
            for (String postDataKey : postData.keySet()) {
                parameters += postDataKey + "=" + postData.get(postDataKey) + "&";
            }
            parameters = parameters.substring(0, parameters.length() - 1);
        }
        byte[] postInfo = parameters.getBytes(StandardCharsets.UTF_8);
        int postDataLength = postInfo.length;

        connection.setRequestMethod("POST");
        // connection.setRequestProperty("Referer", Auth.arcFormUrl);
        connection.setRequestProperty("charset", "utf-8");
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        connection.setRequestProperty("Content-Length", Integer.toString(postDataLength));
        connection.setDoOutput(true);

        // logger.info(parameters);
        DataOutputStream wr = new DataOutputStream(connection.getOutputStream());
        wr.writeBytes(parameters);
        wr.flush();
        wr.close();
        return make(connection);
    }

    public String make(HttpURLConnection urlConnection) throws IOException {
        logger.info("Making request at " + urlConnection.getURL());

        int responseCode = urlConnection.getResponseCode();
        logger.info("Response " + responseCode);

        InputStream gzipStream = new GZIPInputStream(urlConnection.getInputStream());
        Reader decoder = new InputStreamReader(gzipStream, "UTF-8");
        BufferedReader reader = new BufferedReader(decoder);
        String line;
        StringBuilder response = new StringBuilder();
        while ((line = reader.readLine()) != null) {
            response.append(line);
        }

        reader.close();
        return response.toString();
    }

//    public void dumpCookies() throws IOException {
//        logger.info("Cookies :");
//
//        File cookieFile = new File("cookies.txt");
//        FileWriter fileWriter = new FileWriter(cookieFile);
//        for (HttpCookie cookie : cookieManager.getCookieStore().getCookies()) {
//            if (Auth.gameUrl.contains(cookie.getDomain())) {
//                StringBuilder stringBuilder = new StringBuilder();
//                stringBuilder.append(cookie.getDomain() + "|" +
//                        cookie.getName() + "|" +
//                        cookie.getValue() + "|" +
//                        cookie.getVersion() + "|" +
//                        cookie.getComment() + "|" +
//                        cookie.getCommentURL() + "|" +
//                        cookie.getPath() + "|" +
//                        cookie.getPortlist() + "|" +
//                        cookie.getDiscard() + "|" +
//                        cookie.getMaxAge() + "|" +
//                        cookie.getSecure() + "\n");
//                fileWriter.write(stringBuilder.toString());
//            }
//        }
//            fileWriter.close();
//    }
//
//    public void loadCookies() throws IOException {
//        File cookieFile = new File("cookies.txt");
//        if (cookieFile.exists()) {
//            BufferedReader reader = new BufferedReader( new FileReader (cookieFile));
//            String         line = null;
//            while( ( line = reader.readLine() ) != null ) {
//                if (Auth.gameUrl.contains(line.split("\\|")[0])) {
//                    HttpCookie cookie = new HttpCookie(line.split("\\|")[1], line.split("\\|")[2]);
//                    cookie.setDomain(line.split("\\|")[1]);
//                    logger.info(line.split("\\|")[3]);
//                    cookie.setVersion(Integer.parseInt(line.split("\\|")[3]));
//                    cookie.setMaxAge(Long.parseLong(line.split("\\|")[9]));
//                    cookie.setPath(line.split("\\|")[6]);
//                    cookieManager.getCookieStore().add(URI.create(cookie.getDomain()), cookie);
//                }
//            }
//        }
//    }
}
