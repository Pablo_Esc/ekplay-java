package me.pogorzelski.ekplay;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;
import com.eclipsesource.json.JsonValue;

public class User implements Job {

    public static String name;
    public static String xp;
    public static String gold;
    public static String gems;
    public static List<Deck> decks;

    public static final Logger logger = LogManager.getLogger(Logger.class
            .getName());

    @SuppressWarnings("restriction")
    public static void updateUserData() throws IOException,
            InterruptedException {
        if (Config.isAuth) {
            Game game = Config.getGame();
            String userData = game.makeRegularGameRequest("user", "GetUserInfo", null, Boolean.FALSE);
            JsonObject userJson = Json.parse(userData).asObject().get("data").asObject();
            name = Util.getJsonData(userJson, "NickName");
            xp = Util.getJsonData(userJson, "Exp");
            gold = Util.getJsonData(userJson, "Coins");
            gems = Util.getJsonData(userJson, "Cash");

            StringBuilder builder = new StringBuilder();
            builder.append("Nick : ").append(name).append("\n");
            builder.append("Experience : ").append(xp).append("\n");
            builder.append("Gold : ").append(gold).append("\n");
            builder.append("Gems : ").append(gems).append("\n");

            Config.textAreaPlayerData.setText(builder.toString());
        }
    }

    public static void fillUserDecks() throws IOException, InterruptedException, NumberFormatException, SQLException {
        if (Config.isAuth) {
            decks = new ArrayList<>();
            Game game = Config.getGame();
            String decksResponse = game.makeRegularGameRequest("card", "GetCardGroup", null, true);
            JsonArray decksData = Json.parse(decksResponse).asObject().get("data").asObject().get("Groups").asArray();
            for (JsonValue deckData : decksData) {
                logger.info("Got deck data : " + deckData.toString());
                Deck deck = new Deck();
                deck.setId(Integer.valueOf(Util.getJsonData(deckData.asObject(), "GroupId")));
                JsonArray cardArray = deckData.asObject().get("UserCardInfo").asArray();
                for (JsonValue cardData : cardArray) {
                    Card card = DataAccess.cardDao.queryForId(Integer.valueOf(Util.getJsonData(cardData.asObject(), "CardId")));
                    card.setLvl(Util.getJsonData(cardData.asObject(), "Level"));
                    if (!Util.getJsonData(cardData.asObject(), "Evolution").equals("0")) {
                        String evoId = Util.getJsonData(cardData.asObject(), "SkillNew");
                        card.setEvoSkill(DataAccess.skillDao.queryForId(Integer.valueOf(evoId)).getName());
                        logger.info(card.getEvoSkill());
                    }
                    logger.info("Adding card : " + card.getName());
                    deck.addCard(card);
                }
                JsonArray runeArray = deckData.asObject().get("UserRuneInfo").asArray();
                for (JsonValue runeData : runeArray) {
                    Rune rune = DataAccess.runeDao.queryForId(Integer.valueOf(Util.getJsonData(runeData.asObject(), "RuneId")));
                    logger.info("Adding rune : " + rune.getName());
                    deck.addRune(rune);
                }
                decks.add(deck);
            }
        }
    }

    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        // TODO Auto-generated method stub

    }
}
