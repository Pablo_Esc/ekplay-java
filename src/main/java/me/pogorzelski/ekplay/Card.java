package me.pogorzelski.ekplay;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = "cards")
public class Card {

	@DatabaseField(id = true)
	private Integer id;
	@DatabaseField
	private String name;
	@DatabaseField
	private String type;

	private String lvl;

	private String evoSkill;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLvl() {
		return lvl;
	}

	public void setLvl(String lvl) {
		this.lvl = lvl;
	}

	public String getEvoSkill() {
		return evoSkill;
	}

	public void setEvoSkill(String evoSkill) {
		this.evoSkill = evoSkill;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
