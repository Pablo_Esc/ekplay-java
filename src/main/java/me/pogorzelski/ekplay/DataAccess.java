package me.pogorzelski.ekplay;

import java.sql.SQLException;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.table.TableUtils;

public class DataAccess {

    public static Dao<Card, Integer> cardDao = null;
    public static Dao<Skill, Integer> skillDao = null;
    public static Dao<Rune, Integer> runeDao = null;
    public static Dao<CardSkill, Integer> cardSkillDao = null;

    public DataAccess() throws SQLException {
        cardDao = DaoManager.createDao(Config.connectionSource, Card.class);
        TableUtils.createTableIfNotExists(Config.connectionSource, Card.class);
        skillDao = DaoManager.createDao(Config.connectionSource, Skill.class);
        TableUtils.createTableIfNotExists(Config.connectionSource, Skill.class);
        runeDao = DaoManager.createDao(Config.connectionSource, Rune.class);
        TableUtils.createTableIfNotExists(Config.connectionSource, Rune.class);
        cardSkillDao = DaoManager.createDao(Config.connectionSource, CardSkill.class);
        TableUtils.createTableIfNotExists(Config.connectionSource, CardSkill.class);
    }

}
