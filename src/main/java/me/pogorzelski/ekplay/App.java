package me.pogorzelski.ekplay;

import java.io.IOException;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.SchedulerException;

@SuppressWarnings("restriction")
public class App extends Application {

	public static Stage currentStage;
	public static final Logger logger = LogManager.getLogger(Logger.class
			.getName());

	public static void main(String[] args) throws IOException,
			ConfigurationException {
		launch(args);
	}

	@Override
	public void start(Stage stage) throws Exception {
		Parent root = FXMLLoader
				.load(getClass().getResource("/fxml/main.fxml"));
		Scene scene = new Scene(root);
		scene.getStylesheets().add("styles/main.css");
		stage.setTitle("EK Play");
		stage.setScene(scene);
		App.currentStage = stage;
		Config config = new Config();
//		logger.info(config.getCurrent().toString());
//		logger.info(config.getCurrent().getGame());
//		logger.info(Config.getCurrent().toString());
		logger.info(Config.getCurrent().getGame());
		logger.info(Config.getGame().request);

		logger.info("Showing screen");
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {

			@Override
			public void handle(WindowEvent arg0) {
				try {
					Config.scheduler.shutdown();
				} catch (SchedulerException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		stage.show();

	}
}
