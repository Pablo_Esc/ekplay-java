package me.pogorzelski.ekplay;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Deck {

    private Integer id;
    private List<Card> cards;
    private List<Rune> runes;

    public static final Logger logger = LogManager.getLogger(Logger.class.getName());

    public Deck() {
        cards = new ArrayList<>();
        runes = new ArrayList<>();
    }

    public Integer getId() {
        return id;
    }

    @Override
    public String toString() {
        String formatedData = "";
        try {
            formatedData = getFormatedString();
        } catch (SQLException ex) {
            java.util.logging.Logger.getLogger(Deck.class.getName()).log(Level.SEVERE, null, ex);
        }
        return formatedData;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<Card> getCards() {
        return cards;
    }

    public void setCards(List<Card> cards) {
        this.cards = cards;
    }

    public void addCard(Card card) {
        this.cards.add(card);
    }

    public List<Rune> getRunes() {
        return runes;
    }

    public void setRunes(List<Rune> runes) {
        this.runes = runes;
    }

    public void addRune(Rune rune) {
        this.runes.add(rune);
    }

    public String getFormatedString() throws SQLException {
        StringBuilder builder = new StringBuilder();
//        builder.append("Cards : \n");
        Integer cardCount = 1;
        for (Card card : cards) {
            builder.append(card.getName());
            if (card.getEvoSkill() != null) {
                builder.append(":").append(card.getEvoSkill());
            }
            if (cardCount % 5 == 0) {
                builder.append("\n");
            } else {
                builder.append(", ");

            }
            cardCount++;
//            List<CardSkill> cardSkills = DataAccess.cardSkillDao.queryForEq("card_id", card.getId());
//            for (CardSkill cardSkill : cardSkills) {
//                Skill skill = DataAccess.skillDao.queryForId(cardSkill.getSkill().getId());
//                builder.append(skill.getName()).append(" / ");
//            }
//            builder.delete(builder.length() - 3, builder.length());
        }
        if (!builder.toString().endsWith("\n")) {
            builder.append("\n");
        }
//        builder.delete(builder.length() - 2, builder.length());
//        builder.append("\n");
//        builder.append("Runes : ");
        runes.stream().forEach((rune) -> {
            builder.append(rune.getName()).append(" / ");
        });
        builder.delete(builder.length() - 3, builder.length());
        return builder.toString();
    }

    public static String getDeckId(String deckType) {
        if (Config.propertiesHandle.containsKey(deckType) && Config.propertiesHandle.getProperty(deckType) != null) {
            return String.valueOf(Config.propertiesHandle.getProperty(deckType));
        }
        return null;
    }

    public static void setDeck(String deckType, Game game) throws IOException, InterruptedException {
        String deckId = getDeckId(deckType);
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("GroupId", deckId);
        game.makeRegularGameRequest("card", "SetDefalutGroup", parameters, true);
    }
}
