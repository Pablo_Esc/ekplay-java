/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.pogorzelski.ekplay;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonValue;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import static me.pogorzelski.ekplay.Thief.logger;
import static me.pogorzelski.ekplay.Thief.thiefJob;

import org.quartz.*;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 *
 * @author pablo
 */
@PersistJobDataAfterExecution
public class Arena implements InterruptableJob {
    
    private List<Integer> competitorIds = new ArrayList<>();
    private Integer resetTime = 0;
    public static final Integer scheduleTime = 20;
    public static JobDetail arenaJob = null;

    /**
     * @return the resetTime
     */
    public Integer getResetTime() {
        return resetTime;
    }

    /**
     * @param resetTime the resetTime to set
     */
    public void setResetTime(Integer resetTime) {
        this.resetTime = resetTime;
    }
    
    public List<Integer> getCompetitorIds() {
        return competitorIds;
    }
    
    public void setCompetitorIds(List<Integer> competitorIds) {
        this.competitorIds = competitorIds;
    }
    
    public void addCompetitorId(Integer competitorId) {
        this.competitorIds.add(competitorId);
    }
    
    @Override
    public void interrupt() throws UnableToInterruptJobException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public void init(Game game) throws SchedulerException, IOException, InterruptedException {
        logger.info("Init Arena");
        Util.addArenaLog("Scheduling arena fights");
        //schedule thief hunting
        arenaJob = newJob(Arena.class).withIdentity("arenaJob", "arenaGroup").storeDurably(true).build();
        arenaJob.getJobDataMap().put("game", game);
        Config.scheduler.addJob(arenaJob, true);
        scheduleArena();
    }

    private void scheduleArena() throws SchedulerException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, 30);
        Trigger triggerArena = newTrigger().withIdentity("arena", "arenaGroup").withSchedule(SimpleScheduleBuilder.repeatMinutelyForever(scheduleTime)).startAt(calendar.getTime()).forJob(arenaJob).build();
        Config.scheduler.scheduleJob(triggerArena);
    }

    public void getCompetitors(Game game) throws IOException, InterruptedException {
        Util.addArenaLog("Searching for competitors");
        String competitorsData = game.makeRegularGameRequest("arena", "GetRankCompetitors", null, Boolean.TRUE);
        JsonArray competitorsJson = Json.parse(competitorsData).asObject().get("data").asObject().get("Competitors").asArray();
        for (JsonValue competitorData : competitorsJson) {
            Integer competitorId = Integer.valueOf(Util.getJsonData(competitorData.asObject(), "Rank"));
            addCompetitorId(competitorId);
        }
        Integer waitTime = Integer.valueOf(Util.getJsonData(Json.parse(competitorsData).asObject().get("data").asObject(), "Countdown"));
        setResetTime(waitTime);
        arenaJob.getJobDataMap().put("waitTime", waitTime);
        Util.addArenaLog("Got arena competitors list");
    }
    
    public void attackCompetitor(Game game) throws IOException, InterruptedException {
        Util.addArenaLog("Attacking competitor");
        Deck.setDeck("arena-deck", game);
        Integer pickedCompetitor = getCompetitorIds().get(0);
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("CompetitorRank", pickedCompetitor.toString());
        String arenaAttack = game.makeRegularGameRequest("arena", "RankFight", parameters, Boolean.TRUE);
        JsonValue arenaJson = Json.parse(arenaAttack);
        String winStatus = Util.getJsonData(arenaJson.asObject().get("data").asObject(), "Win");
        String expGained = Util.getJsonData(arenaJson.asObject().get("data").asObject().get("ExtData").asObject().get("Award").asObject(), "Exp");
        String goldGained = Util.getJsonData(arenaJson.asObject().get("data").asObject().get("ExtData").asObject().get("Award").asObject(), "Coins");
        String contributionGained = Util.getJsonData(arenaJson.asObject().get("data").asObject().get("ExtData").asObject().get("Award").asObject(), "Resources");
        String competitorName = Util.getJsonData(arenaJson.asObject().get("data").asObject().get("DefendPlayer").asObject(), "NickName");
        
        StringBuilder builder = new StringBuilder();
        builder.append("Arena attack against ").append(competitorName).append(" results : ");
        if (winStatus.equals("1")) {
            builder.append("Battle won. ");
        } else {
            builder.append("Battle lost. ");
        }
        builder.append("Exp gained : ").append(expGained).append(". Gold gained : ").append(goldGained).append(". Contribution gained : ").append(contributionGained).append(".");
        Util.addArenaLog(builder.toString());
    }
    
    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        Game game = (Game) jec.getJobDetail().getJobDataMap().get("game");
        try {
            getCompetitors(game);
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        if (getResetTime() <= 0) {
            try {
                attackCompetitor(game);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            logger.info("Still got to wait " + getResetTime());
        }
    }
    
}
