package me.pogorzelski.ekplay;

import java.io.IOException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonObject;
import java.text.ParseException;

public class Auth {

    public static final String arcFormUrl = "http://mobile.arcgames.com/user/login?gameid=51&sdkvcode=1.3.6&platform=android&androidos=18";
    public static final String arcLoginUrl = "http://mobile.arcgames.com/user/login/?gameid=51";
    public static final String ekLoginUrl = "http://master.ek.ifreeteam.com/mpassport.php?do=plogin&phpp=ANDROID_ARC&phpl=EN&pvc=1.7.4&pvb=2015-08-07%2018%3A55&v=";

    /*
     * Data acquired for login
     */
    public String arcId = "";

    public static final Logger logger = LogManager.getLogger(Logger.class.getName());

    public void process(Request request) throws IOException, InterruptedException, ParseException {
        logger.info("Processing authentication");
        // request.loadCookies();
        // if (request.cookieManager.getCookieStore().getCookies().size() == 0)
        // {
        getArcId(request);
        Thread.sleep(Config.getRequestWaitTime("regular"));
        login(request);
        Thread.sleep(Config.getRequestWaitTime("regular"));
        Config.calcTimeOffset();
        // }
    }

    public void getArcId(Request request) throws IOException {
        logger.info("Getting ArcId");

        // scraping page to get dom id's
        String loginId = "";
        String passwordId = "";
        String arcPageData = request.makeGet(arcFormUrl);
        Pattern loginIdPattern = Pattern.compile("id=\"un\" name=\"(.*?)\"");
        Pattern passwordIdPattern = Pattern.compile("id=\"pw\" name=\"(.*?)\"");
        Matcher loginMatcher = loginIdPattern.matcher(arcPageData);
        Matcher passwordMatcher = passwordIdPattern.matcher(arcPageData);
        if (loginMatcher.find()) {
            loginId = loginMatcher.group(1);
        }
        if (passwordMatcher.find()) {
            passwordId = passwordMatcher.group(1);
        }

        HashMap<String, String> postData = new HashMap<String, String>();
        postData.put(loginId, Config.propertiesHandle.getProperty("login").toString());
        postData.put(passwordId, Config.propertiesHandle.getProperty("password").toString());

        // request for arcId
        String arcLogin = request.makePost(arcLoginUrl, postData);
        // logger.info(arcLogin);
        JsonObject jsonData = Json.parse(arcLogin).asObject();
        arcId = jsonData.getString("loginstatus", "NaN").split(":")[1];
        // logger.info(arcId);
    }

    public void login(Request request) throws IOException, InterruptedException {
        HashMap<String, String> passportData = new HashMap<String, String>();
        passportData.put("plat", "pwe");
        passportData.put("Udid", "");
        passportData.put("uin", arcId);
        passportData.put("nickName", arcId);

        // request for arcId
        String passportResp = request.makePost(ekLoginUrl, passportData);
        // logger.info(passportResp);
        JsonObject jsonData = Json.parse(passportResp).asObject();
        JsonObject userData = jsonData.get("data").asObject().get("uinfo").asObject();
        Config.gameUrl = jsonData.get("data").asObject().get("current").asObject().get("GS_IP").asString();
        // logger.info(userData);
        // logger.info(Auth.gameUrl);
        Thread.sleep(Config.getRequestWaitTime("regular"));

        HashMap<String, String> loginData = new HashMap<String, String>();
        loginData.put("plat", "pwe");
        loginData.put("Udid", "");
        loginData.put("uin", arcId);
        loginData.put("nick", arcId);
        loginData.put("Devicetoken", "");
        loginData.put("time", String.valueOf(userData.getInt("time", 0)));
        loginData.put("access token", userData.getString("access_token", ""));
        loginData.put("uin", userData.getString("uin", ""));
        loginData.put("MUid", userData.getString("MUid", ""));
        loginData.put("sign", userData.getString("sign", ""));
        loginData.put("ppsign", userData.getString("ppsign", ""));
        loginData.put("Origin", "_ARC");

        String loginResp = request.makePost(Config.gameUrl + "login.php?do=mpLogin&phpp=ANDROID_ARC&phpl=EN&pvc=1.7.4&pvb=2015-08-07%2018%3A55&v=1000", loginData);

        Config.isAuth = true;
        // logger.info(loginResp);

        // request.dumpCookies();
    }
}
