package me.pogorzelski.ekplay;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.SchedulerException;
import org.quartz.Trigger;

import static org.quartz.JobBuilder.*;
import static org.quartz.TriggerBuilder.*;
import static org.quartz.CalendarIntervalScheduleBuilder.*;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonValue;

import java.util.HashMap;
import java.util.UUID;
import java.util.logging.Level;

import org.quartz.InterruptableJob;
import org.quartz.PersistJobDataAfterExecution;
import org.quartz.UnableToInterruptJobException;

@PersistJobDataAfterExecution
public class DemonInvasion implements InterruptableJob {

    private String name;
    private Integer hpLeft;
    private Integer timeLeft;
    private Integer meritMade;
    private Integer xpMade;
    private Integer goldMade;
    private Integer cooldownTime;
    private Boolean exitCondition;
    private Boolean isRunning;
    private Integer rank;
    private Integer retries;

    public static JobDetail diJob = null;

    public static final Logger logger = LogManager.getLogger(Logger.class.getName());

    public Boolean getIsRunning() {
        return isRunning;
    }

    public void setIsRunning(Boolean isRunning) {
        this.isRunning = isRunning;
    }

    public Boolean getExitCondition() {
        return exitCondition;
    }

    public void setExitCondition(Boolean exitCondition) {
        this.exitCondition = exitCondition;
    }

    public Integer getTimeLeft() {
        return timeLeft;
    }

    public void setTimeLeft(Integer timeLeft) {
        this.timeLeft = timeLeft;
    }

    public Integer getMeritMade() {
        return meritMade;
    }

    public void setMeritMade(Integer meritMade) {
        this.meritMade = meritMade;
    }

    public Integer getXpMade() {
        return xpMade;
    }

    public void setXpMade(Integer xpMade) {
        this.xpMade = xpMade;
    }

    public Integer getGoldMade() {
        return goldMade;
    }

    public void setGoldMade(Integer goldMade) {
        this.goldMade = goldMade;
    }

    public Integer getCooldownTime() {
        return cooldownTime;
    }

    public void setCooldownTime(Integer cooldownTime) {
        this.cooldownTime = cooldownTime;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the hpLeft
     */
    public Integer getHpLeft() {
        return hpLeft;
    }

    /**
     * @param hpLeft the hpLeft to set
     */
    public void setHpLeft(Integer hpLeft) {
        this.hpLeft = hpLeft;
    }

    /**
     * @return the rank
     */
    public Integer getRank() {
        return rank;
    }

    /**
     * @param rank the rank to set
     */
    public void setRank(Integer rank) {
        this.rank = rank;
    }


    public Integer getRetries() {
        return retries;
    }

    public void setRetries(Integer retries) {
        this.retries = retries;
    }

    public void resetRetries() {
        setRetries(0);
    }

    public DemonInvasion() {
        setExitCondition(false);
        setGoldMade(0);
        setMeritMade(0);
        setXpMade(0);
        setRank(1000);
        setIsRunning(false);
    }

    public void fight(Game game) throws IOException, InterruptedException {
        logger.info("fighting boss");
        String fightData = null;
        JsonValue bossFight = null;
        //try to fight
        try {
            fightData = game.makeFastGameRequest("boss", "Fight", null, true);
            bossFight = Json.parse(fightData);
        } catch (Exception e) {
            Util.addDiLog("Network problem fighting boss. Will retry in 10 seconds.");
        }
        //try to set the cooldown
        try {
            setCooldownTime(bossFight.asObject().get("data").asObject().getInt("CanFightTime", 200) * 1000);
        } catch (Exception e) {
            setCooldownTime(10000);
        }
    }

    public void getDemonData(Game game) throws IOException, InterruptedException, NumberFormatException, SQLException {
        try {
            logger.info("getting demon data");
            JsonValue bossInfo = Json.parse(game.makeFastGameRequest("boss", "GetBoss", null, true));
            String bossId = bossInfo.asObject().get("data").asObject().get("Boss").asObject().getString("BossCardId", "NaN");
            String bossHpLeft = Util.getJsonData(bossInfo.asObject().get("data").asObject().get("Boss").asObject(), "BossCurrentHp");
            setHpLeft(Integer.valueOf(bossHpLeft));
            String bossFleeTime = Util.getJsonData(bossInfo.asObject().get("data").asObject(), "BossFleeTime");
            setTimeLeft(Integer.valueOf(bossFleeTime));

            HashMap<String, String> parameters = new HashMap<>();
            parameters.put("Amount", "10");
            parameters.put("StartRank", "1");
            JsonValue rankInfo = Json.parse(game.makeFastGameRequest("boss", "GetRanks", parameters, true));
            String currentRank = Util.getJsonData(rankInfo.asObject().get("data").asObject(), "Rank");
            setRank(Integer.valueOf(currentRank));
            Card demon = DataAccess.cardDao.queryForId(Integer.valueOf(bossId));
            if (!demon.getName().equals("NaN")) {
                setName(demon.getName());
            }
            logger.info(getHpLeft() + " / " + getTimeLeft());
            if ((getHpLeft() == 0 || getTimeLeft() == 0) && getIsRunning()) {
                setExitCondition(true);
                setIsRunning(false);
            } else if ((getHpLeft() == 0 || getTimeLeft() == 0)) {
                setIsRunning(false);
            } else {
                setIsRunning(true);
            }
            resetRetries();
        } catch (IOException | InterruptedException | NumberFormatException | SQLException e) {
            if (getRetries() < 4) {
                setRetries(getRetries() + 1);
                Thread.sleep(3000);
                getDemonData(game);
            }
        }

    }

    public void getFightResult(Game game) throws IOException, InterruptedException {
        logger.info("Getting fight results");
        try {
            JsonValue bossFight = Json.parse(game.makeFastGameRequest("boss", "GetFightData", null, true));
            JsonValue bossFightResult = bossFight.asObject().get("data").asObject().get("ExtData").asObject().get("Award").asObject();
            StringBuilder builder = new StringBuilder();
            builder.append("You've hit ").append(this.getName()).append(" for : ");

            Integer mertitMade = Integer.valueOf(Util.getJsonData(bossFightResult.asObject(), "Honor"));
            builder.append(mertitMade).append(" merit,");
            diJob.getJobDataMap().put("meritMade", getMeritMade() + mertitMade);
            setMeritMade(getMeritMade() + mertitMade);

            Integer goldMade = Integer.valueOf(Util.getJsonData(bossFightResult.asObject(), "Coins"));
            builder.append(goldMade).append(" gold,");
            diJob.getJobDataMap().put("goldMade", getGoldMade() + goldMade);
            setGoldMade(getGoldMade() + goldMade);

            Integer expMade = Integer.valueOf(Util.getJsonData(bossFightResult.asObject(), "Exp"));
            builder.append(expMade).append(" exp.");
            diJob.getJobDataMap().put("xpMade", getXpMade() + expMade);
            setXpMade(getXpMade() + expMade);

            Util.addDiLog(builder.toString());

        } catch (Exception e) {
            Util.addDiLog("Network connection problem getting fight results.");
        }
    }

    public void setDeck(Game game) throws IOException, InterruptedException {
        try {
            String deckToSet = (String) Config.propertiesHandle.getProperty(getName());
            HashMap<String, String> parameters = new HashMap<String, String>();
            parameters.put("GroupId", deckToSet);
            game.makeRegularGameRequest("card", "SetDefalutGroup", parameters, true);
            diJob.getJobDataMap().put("deckSet", true);
            resetRetries();
        } catch (Exception e) {
            if (getRetries() < 4) {
                setRetries(getRetries() + 1);
                Thread.sleep(3000);
                setDeck(game);
            }
        }
    }

    public void init(Game game) throws NumberFormatException, IOException, InterruptedException, SQLException, SchedulerException {
        logger.info("Init DI");
        //schedule regular DIs
        diJob = newJob(DemonInvasion.class).withIdentity("diJob", "diGroup").storeDurably(true).build();
        diJob.getJobDataMap().put("game", game);
        diJob.getJobDataMap().put("deckSet", false);
        diJob.getJobDataMap().put("xpMade", 0);
        diJob.getJobDataMap().put("goldMade", 0);
        diJob.getJobDataMap().put("meritMade", 0);
        Config.scheduler.addJob(diJob, true);
        scheduleDi();

        //check if DI is running
        getDemonData(game);
        if (isRunning) {
            logger.info("We've got a live");
            Calendar calendar = Calendar.getInstance();
            calendar.add(Calendar.SECOND, 3);
            Trigger triggerInstantDi = newTrigger().withIdentity("instantDi", "diGroup").startAt(calendar.getTime()).forJob(diJob).build();
            Config.scheduler.scheduleJob(triggerInstantDi);
        }
    }

    public void scheduleDi() throws SchedulerException {
        Calendar calendar = Calendar.getInstance();
        Calendar calendarCurrent = Calendar.getInstance();
        calendar.set(Calendar.HOUR_OF_DAY, 5);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 5);
        calendar.set(Calendar.MILLISECOND, 0);
        calendar.setTimeInMillis(calendar.getTimeInMillis() + Config.getTimeOffset());

        if (calendar.getTimeInMillis() < calendarCurrent.getTimeInMillis()) {
            calendar.add(Calendar.DATE, 1);
        }
        Trigger triggerFirstDi = newTrigger().withIdentity("firstDi", "diGroup").withSchedule(calendarIntervalSchedule().withIntervalInDays(1)).startAt(calendar.getTime()).forJob(diJob).build();
        Config.scheduler.scheduleJob(triggerFirstDi);
        Util.addGeneralLog("DI scheduled at " + calendar.toString());
        calendar.set(Calendar.DATE, calendarCurrent.get(Calendar.DATE));

        calendar.add(Calendar.HOUR, 8);
        if (calendar.getTimeInMillis() < calendarCurrent.getTimeInMillis()) {
            calendar.add(Calendar.DATE, 1);
        }
        Trigger triggerSecondDi = newTrigger().withIdentity("secondDi", "diGroup").withSchedule(calendarIntervalSchedule().withIntervalInDays(1)).startAt(calendar.getTime()).forJob(diJob).build();
        Config.scheduler.scheduleJob(triggerSecondDi);
        Util.addGeneralLog("DI scheduled at " + calendar.toString());
        calendar.set(Calendar.DATE, calendarCurrent.get(Calendar.DATE));

        calendar.add(Calendar.HOUR, 8);
        if (calendar.getTimeInMillis() < calendarCurrent.getTimeInMillis()) {
            calendar.add(Calendar.DATE, 1);
        }
        Trigger triggerThirdDi = newTrigger().withIdentity("thirdDi", "diGroup").withSchedule(calendarIntervalSchedule().withIntervalInDays(1)).startAt(calendar.getTime()).forJob(diJob).build();
        Config.scheduler.scheduleJob(triggerThirdDi);
        Util.addGeneralLog("DI scheduled at " + calendar.toString());
    }

    public void finishDi() throws UnableToInterruptJobException, SchedulerException {
        StringBuilder builder = new StringBuilder();
        builder.append("DI against ").append(getName()).append(" finished with : \n");
        builder.append("XP : ").append(getXpMade()).append("\n");
        builder.append("Gold made : ").append(getGoldMade()).append("\n");
        builder.append("Merit made : ").append(getMeritMade()).append("\n");
        builder.append("Final rank : ").append(getRank()).append("\n");
        Util.addDiLog(builder.toString());

        diJob.getJobDataMap().put("deckSet", false);
        diJob.getJobDataMap().put("xpMade", 0);
        diJob.getJobDataMap().put("goldMade", 0);
        diJob.getJobDataMap().put("meritMade", 0);
        logger.info("Clearing triggers");
        for (Trigger trigger : Config.scheduler.getTriggersOfJob(diJob.getKey())) {
            if (trigger.getKey().equals("firstDi") || trigger.getKey().equals("secondDi") || trigger.getKey().equals("thirdDi")) {
                continue;
            } else {
                Config.scheduler.unscheduleJob(trigger.getKey());
            }
        }
        logger.info("Cleaned triggers");

        interrupt();
    }

    public void runRound(Game game) throws IOException, InterruptedException, NumberFormatException, SQLException, SchedulerException {
        logger.info("Running round");
        //getDemonData(game);
        // Long timeChecker = null;
        if (getExitCondition()) {
            finishDi();
        }
        fight(game);
        Calendar timeChecker = Calendar.getInstance();
        logger.info("After fight");
        // timeChecker = new Date().getTime();
        try {
            getFightResult(game);
        } catch (Exception e) {

        }
        Calendar currentTime = Calendar.getInstance();
        // logger.info("Waiting for : " + String.valueOf((getCooldownTime()) -
        // ((new Date().getTime() - timeChecker))) + " miliseconds");
        // Thread.sleep(getCooldownTime() - (new Date().getTime() -
        // timeChecker));
        // logger.info("woken up");
        Date nextRound = new Date(currentTime.getTimeInMillis() - (currentTime.getTimeInMillis() - timeChecker.getTimeInMillis()) + getCooldownTime());
        logger.info("Next round at : " + nextRound);
        Util.addDiLog("Current rank : " + getRank().toString());
        if (exitCondition.equals(Boolean.FALSE)) {
            scheduleRound(nextRound);
        }
    }

    public void scheduleRound(Date date) throws SchedulerException {
        logger.info("Round scheduled at : " + date);
        UUID uuid = UUID.randomUUID();
        Trigger trigger = newTrigger().withIdentity(uuid.toString(), "diGroup").forJob(diJob).startAt(date).build();
        Config.scheduler.scheduleJob(trigger);
    }

    @Override
    public void execute(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        logger.info("Starting job execution");
        try {
            Game game = (Game) diJob.getJobDataMap().get("game");
            Boolean isDeckSet = (Boolean) diJob.getJobDataMap().get("deckSet");
            setXpMade((Integer) diJob.getJobDataMap().get("xpMade"));
            logger.info((Integer) diJob.getJobDataMap().get("xpMade"));
            setGoldMade((Integer) diJob.getJobDataMap().get("goldMade"));
            setMeritMade((Integer) diJob.getJobDataMap().get("meritMade"));
            setIsRunning((Boolean) diJob.getJobDataMap().get("isRunning"));
            getDemonData(game);
            if (!isDeckSet) {
                setDeck(game);
            }
            runRound(game);
            diJob.getJobDataMap().put("isRunning", getIsRunning());
        } catch (IOException | InterruptedException | NumberFormatException | SQLException | SchedulerException ex) {
            java.util.logging.Logger.getLogger(DemonInvasion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void interrupt() throws UnableToInterruptJobException {
        logger.info("leaving job");
    }

}
