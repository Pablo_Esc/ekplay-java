/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package me.pogorzelski.ekplay;

import com.eclipsesource.json.Json;
import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonValue;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.quartz.InterruptableJob;
import org.quartz.JobDetail;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.UnableToInterruptJobException;
import static org.quartz.JobBuilder.newJob;
import org.quartz.PersistJobDataAfterExecution;
import org.quartz.SchedulerException;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 *
 * @author pablo
 */
@PersistJobDataAfterExecution
public class Thief implements InterruptableJob {

    private List<Integer> thiefIds = new ArrayList<>();
    private Integer resetTime = 0;
    public static final Integer scheduleTime = 2;

    public static JobDetail thiefJob = null;

    public static final Logger logger = LogManager.getLogger(Logger.class.getName());

    /**
     * @return the resetTime
     */
    public Integer getResetTime() {
        return resetTime;
    }

    /**
     * @param resetTime the resetTime to set
     */
    public void setResetTime(Integer resetTime) {
        this.resetTime = resetTime;
    }

    /**
     * @return the thiefIds
     */
    public List<Integer> getThiefIds() {
        return thiefIds;
    }

    /**
     * @param thiefIds the thiefIds to set
     */
    public void setThiefIds(List<Integer> thiefIds) {
        this.thiefIds = thiefIds;
    }

    public void addThiefId(Integer thiefId) {
        this.thiefIds.add(thiefId);
    }

    public void init(Game game) throws SchedulerException, IOException, InterruptedException {
        logger.info("Init Thief");
        Util.addThiefLog("Scheduling thief hunting");
        thiefJob = newJob(Thief.class).withIdentity("thiefJob", "thiefGroup").storeDurably(true).build();
        getThieves(game);
        //schedule thief hunting
        thiefJob.getJobDataMap().put("game", game);
        thiefJob.getJobDataMap().put("waitTime", getResetTime());
        Config.scheduler.addJob(thiefJob, true);
        scheduleThief();
    }

    private void scheduleThief() throws SchedulerException {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, 10);
        Trigger triggerThief = newTrigger().withIdentity("thief", "thiefGroup").withSchedule(SimpleScheduleBuilder.repeatMinutelyForever(scheduleTime)).startAt(calendar.getTime()).forJob(thiefJob).build();
        Config.scheduler.scheduleJob(triggerThief);
    }

    public void getThieves(Game game) throws IOException, InterruptedException {
        Util.addThiefLog("Searching for thieves");
        String thievesData = game.makeRegularGameRequest("arena", "GetThieves", null, Boolean.TRUE);
        JsonArray thievesJson = Json.parse(thievesData).asObject().get("data").asObject().get("Thieves").asArray();
        for (JsonValue thiefData : thievesJson) {
            Integer hpCurrent = Integer.valueOf(Util.getJsonData(thiefData.asObject(), "HPCurrent"));
            Integer fleeTime = Integer.valueOf(Util.getJsonData(thiefData.asObject(), "FleeTime"));
            if (hpCurrent > 0 && fleeTime > 0) {
                Integer thiefId = Integer.valueOf(Util.getJsonData(thiefData.asObject(), "UserThievesId"));
                addThiefId(thiefId);
            }
        }
        Integer waitTime = Integer.valueOf(Util.getJsonData(Json.parse(thievesData).asObject().get("data").asObject(), "Countdown"));
        setResetTime(waitTime);
        Util.addThiefLog("Found " + getThiefIds().size() + " thieves");
    }

    public void attackThief(Game game) throws IOException, InterruptedException {
        Util.addThiefLog("Attacking thief");
        Deck.setDeck("thief-deck", game);
        Integer pickedThief = getThiefIds().get(0);
        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("UserThievesId", pickedThief.toString());
        String thiefAttack = game.makeRegularGameRequest("arena", "ThievesFight", parameters, Boolean.TRUE);
        JsonValue thiefJson = Json.parse(thiefAttack);
        String winStatus = Util.getJsonData(thiefJson.asObject().get("data").asObject(), "Win");
        String expGained = Util.getJsonData(thiefJson.asObject().get("data").asObject().get("ExtData").asObject().get("Award").asObject(), "Exp");
        String goldGained = Util.getJsonData(thiefJson.asObject().get("data").asObject().get("ExtData").asObject().get("Award").asObject(), "Coins");

        StringBuilder builder = new StringBuilder();
        builder.append("Thief attack results : ");
        if (winStatus.equals("1")) {
            builder.append("Battle won. ");
        } else {
            builder.append("Battle lost. ");
        }
        builder.append("Exp gained : ").append(expGained).append(". Gold gained : ").append(goldGained).append(".");
        Util.addThiefLog(builder.toString());
    }

    @Override
    public void interrupt() throws UnableToInterruptJobException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void execute(JobExecutionContext jec) throws JobExecutionException {
        Game game = (Game) jec.getJobDetail().getJobDataMap().get("game");
        setResetTime((Integer) thiefJob.getJobDataMap().get("waitTime"));
        try {
            if (getResetTime() - scheduleTime * 60 <= 0) {
                getThieves(game);
                if (getThiefIds().size() > 0) {
                    attackThief(game);
                }
            }
        } catch (IOException | InterruptedException ex) {
            java.util.logging.Logger.getLogger(Thief.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
